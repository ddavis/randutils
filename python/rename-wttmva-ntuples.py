from __future__ import print_function
import os
import argparse
import sys

def parse_args():
    # fmt: off
    parser = argparse.ArgumentParser()
    parser.add_argument("--for-ntup", dest="for_ntup", type=str, help="convert for ntupling")
    parser.add_argument("--for-hist", dest="for_hist", type=str, help="convert for histrogramming")
    parser.add_argument("--dry", dest="dry", action="store_true", help="dry run")
    # fmt: on
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    if args.for_ntup and args.for_hist:
        print("choose one")
        sys.exit(0)

    if args.for_ntup:
        files = os.listdir(args.for_ntup)
        os.chdir(args.for_ntup)
        for f in files:
            if "nominal.bdt_response.root" in f:
                new_name = "{}.root".format(f.split("_nominal.bdt_response.root")[0])
                if args.dry:
                    print("{0: <58}  --->    {1}".format(f, new_name))
                else:
                    os.rename(f, new_name)
        sys.exit(0)


    if args.for_hist:
        files = os.listdir(args.for_hist)
        os.chdir(args.for_hist)
        for f in files:
            if "bdt_response.root" in f:
                continue
            else:
                new_name = "{}_{}".format(f.split('.root')[0], "nominal.bdt_response.root")
                if args.dry:
                    print("{} ---> {}".format(f, new_name))
                else:
                    os.rename(f, new_name)
        sys.exit(0)
